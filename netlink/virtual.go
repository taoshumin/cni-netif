/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package netlink

import (
	"fmt"
	"net"
)

type VirtualNetworkService interface {
	Create(name,ip string) error
}

type VirtualNetwork struct {}

func (n *VirtualNetwork) Create(name,ip string) error  {
	dip:= net.ParseIP(ip)
	if dip == nil{
		return fmt.Errorf("failed to parse device ip %s",ip)
	}

	mg := NewNetifManager([]net.IP{dip})
	ok ,err :=mg.EnsureDummyDevice(name)
	if err!=nil{
		return err
	}

	if ok{
		return fmt.Errorf("device name %s already exists",name)
	}
	return mg.SetUpDummyDevice(name)
}