## netif

简介：主要用于创建虚拟网卡，比如docker创建docker0网卡，默认IP是172.17.0.1。

```shell
docker0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.17.0.1  netmask 255.255.0.0  broadcast 172.17.255.255
        inet6 fe80::42:ddff:fe25:2eea  prefixlen 64  scopeid 0x20<link>
        ether 02:42:dd:25:2e:ea  txqueuelen 0  (Ethernet)
        RX packets 16552608  bytes 25732561130 (25.7 GB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 15576070  bytes 24322449222 (24.3 GB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

## Install

```shell
go get github.com/vishvananda/netlink
```

## Usage 

- deviceName: edgemesh0
- deviceIp: 169.254.96.16

```shell
查看test用例
```

## Linux  

```shell
ip --help
```